/*
package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
import kz.aitu.oop.repository.StudentFileRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

//import kz.aitu.oop.repository.StudentDBRepository;

@RestController
@RequestMapping("/api/task/4")
@AllArgsConstructor
public class AssignmentController4 {


    */
/**
     *
     * @param group
     * @return all student name by group name
     * @throws FileNotFoundException
     *//*

    @GetMapping("/group/{group}")
    public ResponseEntity<?> getStudentsByGroup(@PathVariable("group") String group) throws FileNotFoundException {

        //write your code here
        String result = "";

        return ResponseEntity.ok(result);
    }

    */
/**
     *
     * @param group
     * @return stats by point letter (counting points): example  A-3, B-4, C-1, D-1, F-0
     * @throws FileNotFoundException
     *//*

    @GetMapping("/group/{group}/stats")
    public ResponseEntity<?> getGroupStats(@PathVariable("group") String group) throws FileNotFoundException {

        String result = "";
        int[] stats = new int[5];

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        for (Student student: studentFileRepository.getStudents()) {
            if (group.equals(student.getGroup())) {
                if (student.getPoint() <= 100 && student.getPoint() >= 90) {
                    stats[0]++;
                } else if (student.getPoint() <= 89 && student.getPoint() >= 70) {
                    stats[1]++;
                } else if (student.getPoint() <= 69 && student.getPoint() >= 60){
                    stats[2]++;
                } else if (student.getPoint() <= 59 && student.getPoint() >= 50) {
                    stats[3]++;
                } else {
                    stats[4]++;
                }
            }
        }
        result += "A-" + stats[0];
        result += "B-" + stats[1];
        result += "C-" + stats[2];
        result += "D-" + stats[3];
        result += "F-" + stats[4];

        return ResponseEntity.ok(result);
    }


    */
/**
     *
     * @return top 5 students name by point
     * @throws FileNotFoundException
     *//*

    @GetMapping("/students/top")
    public ResponseEntity<?> getTopStudents() throws FileNotFoundException {

        //write your code here
        String result = "";

        return ResponseEntity.ok(result);
    }
}
*/
