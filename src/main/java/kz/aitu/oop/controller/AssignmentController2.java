/*
package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
import kz.aitu.oop.examples.Point;
//import kz.aitu.oop.repository.StudentDBRepository;
import kz.aitu.oop.repository.StudentFileRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;

@RestController
@RequestMapping("/api/task/2")
@AllArgsConstructor
public class AssignmentController2 {

    //@Autowired
    //private StudentDBRepository studentDBRepository;

    @GetMapping("/students")
    public ResponseEntity<?> getStudents() throws FileNotFoundException {

        //my code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        String result = "";
        for (Student student: studentFileRepository.getStudents()) {
            result += student.getGroup() + "\t" + student.getName() + "\t" + student.getAge() + "\t" + student.getPoint() + "</br>";
        }

        return ResponseEntity.ok(result);
    }

    */
/**
     * Method get all Students from file and calculate average name lengths
     * @return average name length of the all students
     * @throws FileNotFoundException
     *//*

    @GetMapping("/averageStudentsNameLength")
    public ResponseEntity<?> averageStudentsNameLength() throws FileNotFoundException {

        double average = 0;
        //my code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double total = 0;
        int count = 0;
        for (Student student: studentFileRepository.getStudents()) {
            total += student.getName().length();
            count++;
        }
        average = total / count;

        return ResponseEntity.ok(average);
    }


    @GetMapping("/averageStudentsCount")
    public ResponseEntity<?> averageStudentsCount() throws FileNotFoundException {

        double count = 0;
        String group = "";
        int tmp = 0;

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double total = 0;
        for (Student student: studentFileRepository.getStudents()) {
            if (!student.getGroup().equals(group)) {
                group = student.getGroup();
                tmp++;
            }
            total++;
        }
        count = total / tmp;

        return ResponseEntity.ok(count);
    }

    @GetMapping("/averageStudentsPoint")
    public ResponseEntity<?> averageStudentsPoint() throws FileNotFoundException {

        double average = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double total = 0;
        int count = 0;
        for (Student student: studentFileRepository.getStudents()) {
            total += student.getPoint();
            count++;
        }
        average = total / count;

        return ResponseEntity.ok(average);
    }

    @GetMapping("/averageStudentsAge")
    public ResponseEntity<?> averageStudentsAge() throws FileNotFoundException {

        double average = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double total = 0;
        int count = 0;
        for (Student student: studentFileRepository.getStudents()) {
            total += student.getAge();
            count++;
        }
        average = total / count;

        return ResponseEntity.ok(average);
    }

    @GetMapping("/highestStudentsPoint")
    public ResponseEntity<?> highestStudentsPoint() throws FileNotFoundException {

        double maxPoint = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double high = 0;
        for (Student student: studentFileRepository.getStudents()) {
            if(student.getPoint() > high) {
                high = student.getPoint();
            }
        }
        maxPoint = high;

        return ResponseEntity.ok(maxPoint);
    }

    @GetMapping("/highestStudentsAge")
    public ResponseEntity<?> highestStudentsAge() throws FileNotFoundException {

        double maxAge = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double high = 0;
        for (Student student: studentFileRepository.getStudents()) {
            if(student.getAge() > high) {
                high = student.getAge();
            }
        }
        maxAge = high;

        return ResponseEntity.ok(maxAge);
    }

    @GetMapping("/highestGroupAveragePoint")
    public ResponseEntity<?> highestGroupAveragePoint() throws FileNotFoundException {

        double averageGroupPoint = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        String group = "";
        int count = 1;
        double x = 0;
        double y = 0;

        for (Student student: studentFileRepository.getStudents()) {
            if(student.getGroup().equals(group)) {
                x += student.getPoint();
                count++;
            } else {
                group = student.getGroup();
                if (y < x / count) {
                    y = x / count;
                }
                count = 1;
                x = student.getPoint();
            }
        }
        averageGroupPoint = y;

        return ResponseEntity.ok(averageGroupPoint);
    }

    @GetMapping("/highestGroupAverageAge")
    public ResponseEntity<?> highestGroupAverageAge() throws FileNotFoundException {

        double averageGroupAge = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        String group = "";
        int count = 1;
        double x = 0;
        double y = 0;

        for (Student student: studentFileRepository.getStudents()) {
            if(student.getGroup().equals(group)) {
                x += student.getAge();
                count++;
            } else {
                group = student.getGroup();
                if (y < x / count) {
                    y = x / count;
                }
                count = 1;
                x = student.getAge();
            }
        }
        averageGroupAge = y;

        return ResponseEntity.ok(averageGroupAge);
    }


    @GetMapping("/all")
    public ResponseEntity<?> allData() throws FileNotFoundException {

        String result = "";
        result += "averageStudentsNameLength: " + averageStudentsNameLength().getBody() + "</br>";
        result += "averageStudentsCount: " + averageStudentsCount().getBody() + "</br>";
        result += "averageStudentsPoint: " + averageStudentsPoint().getBody() + "</br>";
        result += "averageStudentsAge: " + averageStudentsAge().getBody() + "</br>";
        result += "highestStudentsPoint: " + highestStudentsPoint().getBody() + "</br>";
        result += "highestStudentsAge: " + highestStudentsAge().getBody() + "</br>";
        result += "highestGroupAveragePoint: " + highestGroupAveragePoint().getBody() + "</br>";
        result += "highestGroupAverageAge: " + highestGroupAverageAge().getBody() + "</br>";

        return ResponseEntity.ok(result);
    }
}
*/
