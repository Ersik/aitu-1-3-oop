package kz.aitu.oop.examples.quiz2;

public class FoodFactory {
    public Food getFood(String order) {
        if (order.equalsIgnoreCase("cake")) {
            Food c = new Cake();
            return c;
        }
        else {
            Food p = new Pizza();
            return p;
        }

    }
}
