/*
package kz.aitu.oop.examples;

import java.io.File;
import java.util.Scanner;

public class Shape {

    class Point {
        int x, y;
        Point next;
        public Point(int x, int y) {
            this.x = x;
            this.y = y;
            this.next = null;
        }
    }
    private Point head;

    Shape() {
        head = null;
    }

    public void addPoint(int x, int y) {
        Point node1 = new Point(x, y);
        if (head == null) {
            head = node1;
        } else {
            Point node2 = head;
            while (node2.next != null) {
                node2 = node2.next;
            }
            node2.next = node1;
        }
    }

    public void getPoints() {
        Point node = head;
        while (node.next != null) {
            System.out.println(node.x + ", " + node.y);
            node = node.next;
        }
        System.out.println(node.x + ", " + node.y);
    }

    public double calculatePerimeter() {
        Point node = head;
        double tmp = 0;
        while (node.next != null) {
            tmp += Math.sqrt(Math.pow(node.next.x - node.x, 2) + Math.pow(node.next.y - node.y, 2));
        }
        return tmp;
    }

    public double longest() {
        Point node = head;
        double tmp = 0;
        while (node.next != null) {
            if (tmp < Math.sqrt(Math.pow(node.next.x - node.x, 2) + Math.pow(node.next.y - node.y, 2))) {
                tmp = Math.pow(node.next.x - node.x, 2) + Math.pow(node.next.y - node.y, 2);
            }
            node = node.next;
        }
        if (tmp < Math.sqrt(Math.pow(head.x - node.y, 2) + Math.pow(head.y - node.y, 2))) {
            tmp = Math.pow(head.x - node.x, 2) + Math.pow(head.y - node.y, 2);
        }
        return tmp;
    }

    public void averageLength() {
        Point node = head;
        while (node.next != null) {
            System.out.println(Math.sqrt(Math.pow(head.x - node.x, 2) + Math.pow(head.y - node.y, 2)));
            node = node.next;
        }
        System.out.println(Math.pow(head.x - node.x, 2) + Math.pow(head.y - node.y, 2));
    }
}
*/
