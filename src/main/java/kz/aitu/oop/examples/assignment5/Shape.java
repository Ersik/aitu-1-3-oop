package kz.aitu.oop.examples.assignment5;

public class Shape {
    private String color;
    private boolean filled;

    public Shape() {
        this.color = "green";
        this.filled = true;
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        if (filled == true) {
            return true;
        } else {
            return false;
        }
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public String toString() {
        String Check = "";
        if (isFilled() == false) {
            Check = "not";
        }
        return "A Shape with color of "+ this.color +" and "+ "filled";
    }
}
