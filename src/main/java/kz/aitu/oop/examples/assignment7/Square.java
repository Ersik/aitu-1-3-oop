package kz.aitu.oop.examples.assignment7;

public class Square extends Rectangle {
    protected double side;

    public Square() {
        double side = 1.0;
    }

    public Square(double side) {
        super(side, side);
        this.side = side;
    }

    public Square(double side, String color, boolean filled) {
        super(side, side, color, filled);
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
        this.side = side;
    }

    @Override
    public void setWidth(double side) {
        super.setWidth(side);
        this.side = side;
    }

    @Override
    public void setLength(double side) {
        super.setLength(side);
        this.side = side;
    }

    @Override
    public String toString() {
        return "Shape ->" + super.toString() + ". ";
    }
}
