package kz.aitu.oop.examples.assignment7.Subtask3;

public class Circle implements GeometricObject {
    protected double radius;

    public Circle(double radius) {
        this.radius = 1.0;
    }

    @Override
    public String toString() {
        return "Radius: = " + radius;
    }

    @Override
    public double getPerimeter() {
        return radius * 2 * Math.PI;
    }

    @Override
    public double getArea() {
        return radius * radius * Math.PI;
    }
}
