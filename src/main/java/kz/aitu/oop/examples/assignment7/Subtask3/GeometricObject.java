package kz.aitu.oop.examples.assignment7.Subtask3;

public interface GeometricObject {
    public double getPerimeter();
    public double getArea();
}
