package kz.aitu.oop.examples.assignment7.Subtask3;

public interface Resizable {
    public void resize(int percent);
}
