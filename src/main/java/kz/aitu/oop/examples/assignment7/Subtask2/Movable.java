package kz.aitu.oop.examples.assignment7.Subtask2;

public interface Movable {
    public void moveUp();
    public void moveDown();
    public void moveRight();
    public void moveLeft();
}
