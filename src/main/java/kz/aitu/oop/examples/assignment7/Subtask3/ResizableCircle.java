package kz.aitu.oop.examples.assignment7.Subtask3;

public class ResizableCircle extends Circle implements Resizable {

    public ResizableCircle(double radius) {
        super(radius);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public void resize(int percent) {
        radius = (percent * radius) / 100;
    }
}
