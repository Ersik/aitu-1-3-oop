package kz.aitu.oop.examples.quiz1;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        Singleton singleton = Singleton.getSingleInstance(string);
    }
}
