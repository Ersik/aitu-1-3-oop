package kz.aitu.oop.examples.quiz1;

public class Singleton {

    public static Singleton instance;
    public String str;

    private Singleton() {

    }

    public static Singleton getSingleInstance (String str) {
        if (instance == null) {
            instance = new Singleton();
            System.out.println("Hello I am Singleton! Let me say " + str + "hello world to you");
        }
        return instance;
    }
}